// SPDX-License-Identifier: MIT License
use serde_derive::Deserialize;

#[derive(Debug, Default, Deserialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum Operation {
    And(Vec<Operation>),
    Contains {
        key: String,
        value: String,
    },
    Equals {
        key: String,
        value: String,
    },
    Follow {
        key: String,
        op: Box<Operation>,
    },
    Or(Vec<Operation>),
    Not(Box<Operation>),
    #[default]
    True,
    False,
}

impl Operation {
    pub fn matches(&self, data: &serde_json::Value) -> bool {
        match &self {
            Operation::Contains { key, value } => {
                let v = &data[key];
                match v {
                    serde_json::Value::Null => false,
                    _ => v.is_string() && v.as_str().unwrap().contains(value),
                }
            }
            Operation::Equals { key, value } => {
                let v = &data[key];
                match v {
                    serde_json::Value::Null => false,
                    _ => v.is_string() && v.as_str().unwrap() == value,
                }
            }
            Operation::Follow { key, op } => {
                let v = &data[key];
                match v {
                    serde_json::Value::Null => false,
                    d => op.matches(d),
                }
            }
            Operation::Not(other) => !other.matches(data),
            Operation::True => true,
            Operation::False => false,
            Operation::And(l) => l.iter().fold(true, |acc, v| acc && v.matches(data)),
            Operation::Or(l) => l.iter().fold(false, |acc, v| acc || v.matches(data)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    macro_rules! operation_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
    #[test]
    fn $name() {
        let (condition, result) = $value;
        let json : serde_json::Value = serde_json::from_str(r#"{"name": "John Doe", "multi": {"nested": {"item": "foo"}}}"#).unwrap();
        assert_eq!(condition.matches(&json), result);
    }
            )*
        }
    }

    operation_tests! {
        default_ok: (Operation::True,
            true),
        equals_ok: (Operation::Equals {
                key: String::from("name"),
                value: String::from("John Doe")
            },
            true),
        equals_nok: (Operation::Equals {
                key: String::from("name"),
                value: String::from("JohnDoe")
            },
            false),
        equals_bad_key_nok: (Operation::Equals {
                key: String::from("name2"),
                value: String::from("John Doe"),
            },
            false),
        contains_ok: (Operation::Contains {
                key: String::from("name"),
                value: String::from("hn D"),
            },
            true),
        contains_nok: (Operation::Contains {
                key: String::from("name"),
                value: String::from("hnD"),
            },
            false),
        contains_bad_key_nok: (Operation::Contains {
                key: String::from("_name"),
                value: String::from("hn D"),
            },
            false),
        follow_1_ok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::True),
            },
            true),
        follow_1_nok: (Operation::Follow {
                key: String::from("nulti"),
                op: Box::new(Operation::True),
            },
            false),
        follow_2_ok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("nested"),
                    op: Box::new(Operation::True),
                }),
            },
            true),
        follow_2_nok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("pested"),
                    op: Box::new(Operation::True),
                }),
            },
            false),
        and_1_ok: (Operation::And ([
                Operation::True,
            ].to_vec()),
            true),
        and_1_nok: (Operation::And ([
                Operation::False,
            ].to_vec()),
            false),
        and_2_ok_ok: (Operation::And ([
                Operation::True,
                Operation::True,
            ].to_vec()),
            true),
        and_2_nok_ok: (Operation::And ([
                Operation::False,
                Operation::True,
            ].to_vec()),
            false),
        and_2_ok_nok: (Operation::And ([
                Operation::True,
                Operation::False,
            ].to_vec()),
            false),
        and_2_nok_nok: (Operation::And ([
                Operation::False,
                Operation::False,
            ].to_vec()),
            false),
        or_1_ok: (Operation::Or ([
                Operation::True,
            ].to_vec()),
            true),
        or_1_nok: (Operation::Or ([
                Operation::False,
            ].to_vec()),
            false),
        or_2_ok_ok: (Operation::Or ([
                Operation::True,
                Operation::True,
            ].to_vec()),
            true),
        or_2_nok_ok: (Operation::Or ([
                Operation::False,
                Operation::True,
            ].to_vec()),
            true),
        or_2_ok_nok: (Operation::Or ([
                Operation::True,
                Operation::False,
            ].to_vec()),
            true),
        or_2_nok_nok: (Operation::Or ([
                Operation::False,
                Operation::False,
            ].to_vec()),
            false),
        not_ok: (Operation::Not (Box::new(Operation::False)),
            true),
        not_nok: (Operation::Not (Box::new(Operation::True)),
            false),
    }
}
