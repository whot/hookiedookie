// SPDX-License-Identifier: MIT License
use crate::settings::{Project, Settings, Webhook};

use actix_web::{
    dev::Service as _,
    guard::{self, Guard, GuardContext},
    web::{self, Data, ReqData},
    App, HttpMessage, HttpRequest, HttpResponse, HttpServer,
};

async fn webhook_fn(_req: HttpRequest, config: Webhook, body: serde_json::Value) -> HttpResponse {
    // look for a match
    for function in config.functions {
        if function.rule.matches(&body) {
            return HttpResponse::Ok().body(function.run);
        }
    }
    HttpResponse::NotImplemented().body("Unable to find a match")
}

// Split webhook_fn in 2 to be able to test with a proper Webhook struct
async fn webhook_fn_handler(
    req: HttpRequest,
    data: ReqData<Webhook>,
    body: web::Json<serde_json::Value>,
) -> HttpResponse {
    webhook_fn(req, data.into_inner(), body.into_inner()).await
}

struct GitlabTokenHeader;

impl Guard for GitlabTokenHeader {
    fn check(&self, req: &GuardContext) -> bool {
        // if there is no configuration for this webhook, abort
        if let Some(webhook) = req.req_data().get::<Webhook>() {
            // ensure the header is set and has the correct value
            if let Some(value) = req.head().headers().get("x-gitlab-token") {
                return value.to_str().unwrap() == webhook.token;
            }
        }
        false
    }
}

fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        // bind to the url /webhook/namespace/project
        web::resource("/webhook/{namespace}/{project}")
            // ensure the content type is JSON
            .guard(guard::Header("content-type", "application/json"))
            // add request data extracted from global settings
            .wrap_fn(|req, srv| {
                // we are before the request processing
                let settings = req.app_data::<Data<Settings>>();

                match settings {
                    Some(settings) => {
                        let match_info = req.match_info();
                        match settings.get(
                            // unwrap is fine because if it is not, then
                            // there is a bug in the Actix resource path handler
                            // and we better abort
                            Project::from_name(
                                match_info.get("namespace").unwrap(),
                                match_info.get("project").unwrap(),
                            ),
                        ) {
                            Some(webhook) => req.extensions_mut().insert(webhook),
                            _ => None,
                        };
                    }
                    _ => (),
                };

                // future function for processing the response
                srv.call(req)
            })
            // now handle the post
            .route(
                web::post()
                    // ensure the token is valid this needs to be here
                    // so we are sure the wrap_fn above is called before
                    .guard(GitlabTokenHeader)
                    // call the webhook function
                    .to(webhook_fn_handler),
            ),
    );
}

pub async fn run(settings: Settings) -> std::io::Result<()> {
    let listen_address = settings.listen_address.to_owned();
    let port = settings.port;
    let app_data = web::Data::new(settings);

    // Start the Actix web server
    HttpServer::new(move || {
        App::new()
            // append global settings to the web server
            // unwrap may abort the server, but we are in the init phase
            .app_data(app_data.clone())
            .configure(config)
    })
    .bind((listen_address, port))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::rule::Operation;
    use crate::settings::{Function, Project};
    use actix_web::{http, test, web::Json};
    use serde::Serialize;

    #[derive(Debug, Serialize, Clone)]
    #[allow(unused)]
    struct ExamplePayload {
        pub object_kind: String,
    }

    macro_rules! webhook_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[actix_web::test]
                async fn $name() {
                    let (event, function, result) = $value;
                    let payload = ExamplePayload {
                        object_kind: String::from(event),
                    };
                    let http_req = test::TestRequest::default()
                        .set_json(payload.clone())
                        .to_http_request();
                    let svc_payload = test::TestRequest::default()
                        .set_json(payload)
                        .to_srv_request()
                        .extract::<Json<serde_json::Value>>()
                        .await
                        .unwrap()
                        .into_inner();
                    let req_data = Webhook {
                        project: Project::from_name("testns", "test_project"),
                        token: String::from("test"),
                        functions: [function].to_vec(),
                    };
                    let resp = webhook_fn(http_req, req_data, svc_payload).await;
                    assert_eq!(resp.status(), result);
                }
            )*
        }
    }

    webhook_tests! {
        push_ok: (
            "push",
            Function {
                rule: Operation::Equals {
                    key: String::from("object_kind"),
                    value: String::from("push"),
                },
                run: String::from("test"),
                env: None,
            }, http::StatusCode::OK
        ),
        push_nok: (
            "puch",
            Function {
                rule: Operation::Equals {
                    key: String::from("object_kind"),
                    value: String::from("push"),
                },
                run: String::from("test"),
                env: None,
            }, http::StatusCode::NOT_IMPLEMENTED
        ),
    }
}
