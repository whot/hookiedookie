// SPDX-License-Identifier: MIT License
use crate::rule::Operation;
use config::{Config, ConfigError, Environment, File};
use serde_derive::Deserialize;

#[derive(Debug, Deserialize, Clone)]
pub struct Project {
    pub namespace: String,
    pub project: String,
}

impl Project {
    pub fn from_name(namespace: &str, project: &str) -> Project {
        Project {
            namespace: String::from(namespace),
            project: String::from(project),
        }
    }
}

impl PartialEq for Project {
    fn eq(&self, other: &Self) -> bool {
        self.namespace == other.namespace && self.project == other.project
    }
}

#[derive(Debug, Default, Deserialize, Clone)]
pub struct Function {
    #[serde(default)]
    pub rule: Operation,
    pub run: String,
    #[serde(default)]
    pub env: Option<Vec<String>>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Webhook {
    pub project: Project,
    pub token: String,
    pub functions: Vec<Function>,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub debug: bool,
    pub listen_address: String,
    pub port: u16,
    pub webhooks: Vec<Webhook>,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let s = Config::builder()
            // Start off by merging in the "default" configuration file
            .add_source(File::with_name("Settings"))
            // Add in settings from the environment (with a prefix of APP)
            // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
            .add_source(Environment::with_prefix("app"))
            // You may also programmatically change settings
            .build()?;

        // You can deserialize (and thus freeze) the entire configuration as
        s.try_deserialize()
    }

    pub fn get(&self, project: Project) -> Option<Webhook> {
        for webhook in self.webhooks.iter() {
            if project == webhook.project {
                return Some(webhook.clone());
            }
        }

        None
    }
}
